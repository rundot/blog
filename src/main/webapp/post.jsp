<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
  <head>
  </head>
  <body>
    <form method="post">
      <input type="text" name="topic" placeholder = "topic" /><br />
      <textarea name="content" placeholder="Your post"></textarea>
      <button type="submit" name="submit">Post</button>
    </form>
    <table>
      <thead>
        <tr>
          <th>Topic</th>
          <th>Author</th>
          <th>Creation date</th>
        </tr>
      </thead>
      <tbody>
        <c:forEach var="post" items="${postList}">
          <tr>
            <th><a href="/blog/post/${post.id}">${post.topic}</id></th>
            <th>${post.userId}</th>
            <th>${post.timestamp}</th>
          <tr>
        </c:forEach>
      </tbody>
    </table>
  </body>
</html>