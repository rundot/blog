<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
    <h1>${post.topic}</h1>
    ${post.content}
    <table>
      <thead>
        <tr>
          <th>Author</th>
          <th>Creation date</th>
          <th>Comment</th>
        </tr>
      </thead>
      <tbody>
        <c:forEach var="comment" items="${commentList}">
          <tr>
            <td>${comment.userId}</td>
            <td>${comment.timestamp}</td>
            <td>${comment.content}</td>
          </tr>
        </c:forEach>
      </tbody>
    </table>
    <form method="post">
      <input type="hidden" name="id" value="${post.id}" />
      <textarea name="content" placeholder="Your comment"></textarea>
      <button type="submit" name="submit">Post</button>
    </form>
  </body>
</html>