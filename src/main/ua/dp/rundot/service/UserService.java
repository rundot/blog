package ua.dp.rundot.service;

import ua.dp.rundot.dao.UserDao;
import ua.dp.rundot.dao.postgreSQL.UserDaoImpl;
import ua.dp.rundot.domain.User;

import java.util.List;

/**
 * Created by emaksimovich on 16.01.17.
 */
public class UserService {

    private static UserDao userDao = new UserDaoImpl();

    public static int save(User user) {
        return userDao.save(user);
    }

    public static User get(int id) {
        return userDao.get(id);
    }

    public static List<User> list() {
        return userDao.list();
    }

    public static boolean userValid(String login, String password) {
        return list().stream().filter(user-> (user.getLogin().equals(login) && user.getPassword().equals(password))).count() > 0;
    }

    public static int getIdByLoginPassword(String login, String password) {
        return list().stream().filter(user-> (user.getLogin().equals(login) && user.getPassword().equals(password))).findFirst().get().getId();
    }

}
