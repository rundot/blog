package ua.dp.rundot.service;

import ua.dp.rundot.dao.PostDao;
import ua.dp.rundot.dao.postgreSQL.PostDaoImpl;
import ua.dp.rundot.domain.Post;

import java.util.List;

/**
 * Created by emaksimovich on 16.01.17.
 */
public class PostService {

    private static PostDao postDao = new PostDaoImpl();

    public static int save(Post post) {
        return postDao.save(post);
    }

    public static Post get(int id) {
        return postDao.get(id);
    }

    public static List<Post> list() {
        return postDao.list();
    }

}
