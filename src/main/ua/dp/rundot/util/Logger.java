package ua.dp.rundot.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by emaksimovich on 16.01.17.
 */
public class Logger {

    private static Connection connection = SingleConnection.getConnection();

    public static void log(String message) {
        if (connection == null) return;
        try (PreparedStatement statement = connection.prepareStatement("INSERT INTO log(message) VALUES (?);")){
            statement.setString(1, message);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
