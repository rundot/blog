package ua.dp.rundot.controller;

import ua.dp.rundot.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 * Created by emaksimovich on 16.01.17.
 */
public class LoginServlet extends HttpServlet {

    Pattern loginPattern = Pattern.compile("/blog/login");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestURI = req.getRequestURI();
        if (loginPattern.matcher(requestURI).matches()) {
            req.getRequestDispatcher("login.jsp").forward(req, resp);
            return;
        }
        resp.sendError(404);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        if (UserService.userValid(login, password)) {
            req.getSession().setAttribute("userId", UserService.getIdByLoginPassword(login, password));
            resp.sendRedirect("/blog/post");
        } else {
            resp.sendRedirect("/blog/login");
        }
    }
}
