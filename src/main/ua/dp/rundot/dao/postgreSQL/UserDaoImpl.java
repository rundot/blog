package ua.dp.rundot.dao.postgreSQL;

import ua.dp.rundot.dao.UserDao;
import ua.dp.rundot.domain.User;
import ua.dp.rundot.util.Logger;
import ua.dp.rundot.util.SingleConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emaksimovich on 16.01.17.
 */
public class UserDaoImpl implements UserDao {

    private Connection connection = SingleConnection.getConnection();

    @Override
    public int save(User user) {
        if (connection == null) return 0;
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO users (login, password, fullname) " +
                        "VALUES (?, ?, ?) RETURNING id;"
        )) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getFullname());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            int id = resultSet.getInt("id");
            resultSet.close();
            return id;
        } catch (SQLException e) {
            Logger.log(e.getMessage());
        }
        return 0;
    }

    @Override
    public User get(int id) {
        if (connection == null) return null;
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM users WHERE id = ?;"
        )) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            User user = new User(
                    resultSet.getInt("id"),
                    resultSet.getString("login"),
                    resultSet.getString("password"),
                    resultSet.getString("fullname")
            );
            resultSet.close();
            return user;
        } catch (SQLException e) {
            Logger.log(e.getMessage());
        }
        return null;
    }

    @Override
    public List<User> list() {
        if (connection == null) return null;
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM users;");
            List<User> userList = new ArrayList<>();
            while (resultSet.next()) {
                userList.add(new User(
                        resultSet.getInt("id"),
                        resultSet.getString("login"),
                        resultSet.getString("password"),
                        resultSet.getString("fullname")
                ));
            }
            resultSet.close();
            return userList;
        } catch (SQLException e) {
            Logger.log(e.getMessage());
        }
        return null;
    }

    public static void main(String[] args) {
        UserDao userDao = new UserDaoImpl();
        int id;
        User user = new User(0, "user", "user", "Dummy User");
        System.out.printf("Adding user. Returning id: %d%n", id = userDao.save(user));
        System.out.printf("Getting user with id %d: %s%n", id, userDao.get(id));
        System.out.printf("Listing users: %s%n", userDao.list());
    }

}
